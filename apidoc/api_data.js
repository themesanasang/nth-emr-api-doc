define({ "api": [
  {
    "type": "get",
    "url": "/allergy/getAllergy:hn",
    "title": "ข้อมูลจากตาราง opd_allergy",
    "name": "getAllergy",
    "group": "Allergy",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hn",
            "description": "<p>รหัสประจำตัวผู้ป่วย</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data",
            "description": "<p>ข้อมูล opd_allergy ของผู้ป่วยมีจำนวน 1 แถว ขึ้นไป</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/allergy.js",
    "groupTitle": "Allergy"
  },
  {
    "type": "get",
    "url": "/approve/checkApprove:pid",
    "title": "ตรวจสอบข้อมูลยินยอมการร้องขอดูประวัติ",
    "name": "checkApprove",
    "group": "Approve",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pid",
            "description": "<p>รหัสบัตรประชาชน</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>รหัสประจำตัว HN</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/approve.js",
    "groupTitle": "Approve"
  },
  {
    "type": "post",
    "url": "/auth/doAuth",
    "title": "เข้าสู่ระบบ",
    "name": "duAuth",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>ชื่อผู้ใช้สำหรับเข้าสู่ระบบ (ที่ใช้งานในระบบ HosXP. ของ รพ.)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>รหัสผ่านสำหรับเข้าสู่ระบบ (ที่ใช้งานในระบบ HosXP. ของ รพ.) <em><strong>ทำการเข้ารหัสก่อนเรียกใช้งาน Api</strong></em></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>รหัส token ใช้ในการแนบตรวจสอบข้อมูลในการร้องขอข้อมูลในระบบ</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/clinic/getClinic:hn",
    "title": "ข้อมูลจากตาราง Clinic",
    "name": "getClinic",
    "group": "Clinic",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hn",
            "description": "<p>รหัสประจำตัวผู้ป่วย</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data",
            "description": "<p>ข้อมูล Clinic ของผู้ป่วยมีจำนวน 1 แถว ขึ้นไป</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/clinic.js",
    "groupTitle": "Clinic"
  },
  {
    "type": "get",
    "url": "/lab/getLabHead:vn",
    "title": "ข้อมูล Lab Head",
    "name": "getLabHead",
    "group": "Lab",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "vn",
            "description": "<p>รหัสประจำตัวผู้ป่วยที่มารับบริการในแต่ละครั้ง</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>ข้อมูล Lab Head</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/lab.js",
    "groupTitle": "Lab"
  },
  {
    "type": "get",
    "url": "/lab/getLabOrder:order",
    "title": "ข้อมูล Lab Order",
    "name": "getLabOrder",
    "group": "Lab",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "order",
            "description": "<p>รหัสลำดับของรายการสั่ง Lab</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>ข้อมูล Lab Order</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/lab.js",
    "groupTitle": "Lab"
  },
  {
    "type": "get",
    "url": "/opitemrece/getOpitemrece:vn",
    "title": "ข้อมูล Opitemrece",
    "name": "getOpitemrece",
    "group": "Opitemrece",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "vn",
            "description": "<p>รหัสประจำตัวผู้ป่วยที่มารับบริการในแต่ละครั้ง</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>ข้อมูล Opitemrece รายการยาและรายการค่าใช้จ่ายอื่น ๆ</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/opitemrece.js",
    "groupTitle": "Opitemrece"
  },
  {
    "type": "get",
    "url": "/ovst/getOvst:hn",
    "title": "ข้อมูลจากตาราง Ovst",
    "name": "getOvst",
    "group": "Ovst",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hn",
            "description": "<p>รหัสประจำตัวผู้ป่วย</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data",
            "description": "<p>ข้อมูลประวัติหลัก ๆ ของผู้ป่วยมีจำนวน 1 แถว ขึ้นไป</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/ovst.js",
    "groupTitle": "Ovst"
  },
  {
    "type": "get",
    "url": "/ovst/getOvstDiag:vn",
    "title": "ข้อมูลจากตาราง OvstDiag",
    "name": "getOvstDiag",
    "group": "Ovst",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "vn",
            "description": "<p>รหัสประจำตัวผู้ป่วยในแต่ละครั้งที่มารับบริการ</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data",
            "description": "<p>ข้อมูลรหัส Diag ของผู้ป่วยมีจำนวน 1 แถว ขึ้นไป</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/ovst.js",
    "groupTitle": "Ovst"
  },
  {
    "type": "get",
    "url": "/patient/getPatient:pid",
    "title": "ข้อมูลประวัติจากตาราง patient",
    "name": "getPatient",
    "group": "Patient",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pid",
            "description": "<p>รหัสบัตรประชาชน</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (true)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>ข้อมูลประวัติ</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>สถานะการตอบกลับ (false)</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>ข้อความแสดงผลของสถานะ</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "service/patient.js",
    "groupTitle": "Patient"
  }
] });
