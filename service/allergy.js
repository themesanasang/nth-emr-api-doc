/**

* @api {get} /allergy/getAllergy:hn ข้อมูลจากตาราง opd_allergy

* @apiName getAllergy

* @apiGroup Allergy

*

* @apiParam {String} hn รหัสประจำตัวผู้ป่วย

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String[]} data ข้อมูล opd_allergy ของผู้ป่วยมีจำนวน 1 แถว ขึ้นไป

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/