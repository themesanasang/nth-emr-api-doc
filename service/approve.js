/**

* @api {get} /approve/checkApprove:pid ตรวจสอบข้อมูลยินยอมการร้องขอดูประวัติ

* @apiName checkApprove

* @apiGroup Approve

*

* @apiParam {String} pid รหัสบัตรประชาชน

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String} data รหัสประจำตัว HN

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/