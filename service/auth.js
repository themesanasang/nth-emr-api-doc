/**

* @api {post} /auth/doAuth เข้าสู่ระบบ

* @apiName duAuth

* @apiGroup Auth

*

* @apiParam {String} username ชื่อผู้ใช้สำหรับเข้าสู่ระบบ (ที่ใช้งานในระบบ HosXP. ของ รพ.)

* @apiParam {String} password รหัสผ่านสำหรับเข้าสู่ระบบ (ที่ใช้งานในระบบ HosXP. ของ รพ.) ***ทำการเข้ารหัสก่อนเรียกใช้งาน Api***

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String} data รหัส token ใช้ในการแนบตรวจสอบข้อมูลในการร้องขอข้อมูลในระบบ 

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/