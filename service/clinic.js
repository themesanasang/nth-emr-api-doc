/**

* @api {get} /clinic/getClinic:hn ข้อมูลจากตาราง Clinic

* @apiName getClinic

* @apiGroup Clinic

*

* @apiParam {String} hn รหัสประจำตัวผู้ป่วย

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String[]} data ข้อมูล Clinic ของผู้ป่วยมีจำนวน 1 แถว ขึ้นไป

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/