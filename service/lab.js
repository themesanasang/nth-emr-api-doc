/**

* @api {get} /lab/getLabHead:vn ข้อมูล Lab Head

* @apiName getLabHead

* @apiGroup Lab

*

* @apiParam {String} vn รหัสประจำตัวผู้ป่วยที่มารับบริการในแต่ละครั้ง

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String} data ข้อมูล Lab Head

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/


/**

* @api {get} /lab/getLabOrder:order ข้อมูล Lab Order

* @apiName getLabOrder

* @apiGroup Lab

*

* @apiParam {String} order รหัสลำดับของรายการสั่ง Lab

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String} data ข้อมูล Lab Order

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/