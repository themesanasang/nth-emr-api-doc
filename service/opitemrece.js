/**

* @api {get} /opitemrece/getOpitemrece:vn ข้อมูล Opitemrece

* @apiName getOpitemrece

* @apiGroup Opitemrece

*

* @apiParam {String} vn รหัสประจำตัวผู้ป่วยที่มารับบริการในแต่ละครั้ง

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String} data ข้อมูล Opitemrece รายการยาและรายการค่าใช้จ่ายอื่น ๆ

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/
