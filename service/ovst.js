/**

* @api {get} /ovst/getOvst:hn ข้อมูลจากตาราง Ovst

* @apiName getOvst

* @apiGroup Ovst

*

* @apiParam {String} hn รหัสประจำตัวผู้ป่วย

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String[]} data ข้อมูลประวัติหลัก ๆ ของผู้ป่วยมีจำนวน 1 แถว ขึ้นไป

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/


/**

* @api {get} /ovst/getOvstDiag:vn ข้อมูลจากตาราง OvstDiag

* @apiName getOvstDiag

* @apiGroup Ovst

*

* @apiParam {String} vn รหัสประจำตัวผู้ป่วยในแต่ละครั้งที่มารับบริการ

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String[]} data ข้อมูลรหัส Diag ของผู้ป่วยมีจำนวน 1 แถว ขึ้นไป

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/