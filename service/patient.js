/**

* @api {get} /patient/getPatient:pid ข้อมูลประวัติจากตาราง patient

* @apiName getPatient

* @apiGroup Patient

*

* @apiParam {String} pid รหัสบัตรประชาชน

*

* @apiSuccess {Boolean} status สถานะการตอบกลับ (true)

* @apiSuccess {String} message ข้อความแสดงผลของสถานะ

* @apiSuccess {String} data ข้อมูลประวัติ

*

* @apiError {Boolean} status สถานะการตอบกลับ (false)

* @apiError {String} message ข้อความแสดงผลของสถานะ

*/